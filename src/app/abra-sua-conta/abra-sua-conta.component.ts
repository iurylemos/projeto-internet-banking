import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-abra-sua-conta',
  templateUrl: './abra-sua-conta.component.html',
  styleUrls: ['./abra-sua-conta.component.css']
})
export class AbraSuaContaComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  gotoHome() {
    this.router.navigate(['cadastro-clientes'])
  }
}
