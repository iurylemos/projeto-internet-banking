import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  formLogin;

  constructor(private dialog: MatDialog, private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.formLogin = this.fb.group({
      cpf: ['']
    });
  }

  openDialog() {
    const dialogRef = this.dialog.open(HeaderComponent, {
      height: '350px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  gotoCadastroClientes() {
    this.router.navigate(['cadastro-clientes']);
   }

   gotoHome() {
     this.router.navigate(['home']);
   }

}
